function config() {
    const baseApi = "http://callback-api.local"
    return {

        api: {
            callbacks: `${baseApi}/api/callbacks`,
            login: `${baseApi}/api/authenticate/login`,
            logout: `${baseApi}/api/authenticate/logout`,
        },
    };
}

export default  config ;