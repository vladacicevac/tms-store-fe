import React, { Component } from "react";
import { render } from "react-dom";
import "bootstrap/dist/css/bootstrap.css";
import Header from "./Components/Header";
import "./Public/Css/style.scss";
import CallBackForm from "./Components/CallbackForm";
import CallbackLists from "./Components/CallbackLists";
import Login from "./Components/Login";
import { createHashHistory } from "history";
import AuthenticateState from "./Context/Login/AuthenticateState";
import AddComment from "./Components/CallbackLists/AddComment";
import {
    BrowserRouter as Router,
    Switch,
    Route,
    Link
} from "react-router-dom"
import {AuthenticateContext} from "./Context/Login/AuthenticateContext";



class CallbackApp extends Component{

    render() {
        const hist = createHashHistory();
        return(
            <div>
                <AuthenticateState>
                <Router history={hist}>
                    <div className="container">
                        <div className="row">
                            <div className="col-lg-12">
                                <Header />
                            </div>

                        </div>
                        <div className="row">
                            <div className="col-lg-12">
                                <Switch>

                                    <Route exact path='/' component={CallBackForm} />
                                    <Route path='/callback-list' component={CallbackLists} />
                                    <Route path='/add-comment/:id' component={AddComment} />
                                    <Route path='/login' component={Login} />

                                </Switch>

                            </div>
                        </div>
                    </div>

                </Router>
                </AuthenticateState>
            </div>
        )
    }
}


render(<CallbackApp />, document.getElementById("root"));