import React, { Component } from "react";
import { AuthenticateProvider } from "./AuthenticateContext";
import config from "../../config";
import axios from "axios";
import {Redirect} from "react-router";

class AuthenticateState extends Component {
    state = {
        authUser: null,
        token: null
    };

    componentDidMount() {
        this.loadFromLocalStorage();
    }


    componentWillUnmount() {
        window.removeEventListener("beforeunload", this.saveStateToLocalStorage());

        // saves if component has a chance to unmount
        this.saveStateToLocalStorage();
    }

    login = (data) => {

        this.setState({authUser: data}, () => this.saveStateToLocalStorage());

    };

    logout = () => {
        localStorage.removeItem('auth');
        this.setState({authUser: null});
    };

    loadFromLocalStorage() {
        let authUser = JSON.parse(localStorage.getItem("auth"));

        this.setState(authUser);
    }

    saveStateToLocalStorage = () => {
        // for every item in React state
        setTimeout(() => {
            localStorage.setItem(
                "auth",
                JSON.stringify({
                    authUser: this.state.authUser,
                })
            );
        }, 100);
    };

    isAuth = () => {
        const isLoggedIn = this.state.authUser !== null;

        return isLoggedIn;
    }

    render() {
        return (
            <AuthenticateProvider
                value={{
                    authUser: this.state.authUser,
                    login: this.login,
                    logout: this.logout,
                    isAuth: this.isAuth
                }}
            >
                {this.props.children}
            </AuthenticateProvider>
        );
    }
}

export default AuthenticateState;