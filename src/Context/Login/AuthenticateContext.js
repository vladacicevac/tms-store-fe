import React from "react";

export const AuthenticateContext = React.createContext({
    authUser: null,
    token: null,
    login: () => {},
    logout: () => {}
});

export const AuthenticateProvider = AuthenticateContext.Provider;
export const AuthenticateConsumer = AuthenticateContext.Consumer;