import React, { Component } from "react";
import Nav from "./Navbar/Nav";

class Header extends Component {
    render() {
        return (
            <div>
                <header>
                    <Nav />
                </header>
            </div>
        );
    }
}

export default Header;