import React, { Component } from "react";
import {AuthenticateContext} from "../../../Context/Login/AuthenticateContext";
import config from "../../../config";
import Axios from "axios";



class Nav extends Component {
    static contextType = AuthenticateContext;

    handleLogout = () => {

        const configuration = {
            headers: {'Authorization': "Bearer " + this.context.authUser.access_token}
        };

        Axios.post(
            `${config().api.logout}`,
            [],
            configuration
        ).then((response) => {
            console.log(response)
            this.context.logout()
        }).catch((error) => {
            console.log(error)
        });
    }

    render() {

        return (
            <div>
                <nav className="navbar navbar-expand-md">
                    <div className="d-flex justify-content-between col-md-5">
                        <div className="">
                            <div className="collapse navbar-collapse" id="navbarCollapse">
                                <ul className="navbar-nav mr-auto">
                                    <li className="nav-item active">
                                        <a className="nav-link" href="/">
                                            Callback Form <span className="sr-only">(current)</span>
                                        </a>
                                    </li>
                                    <li className="nav-item active">
                                        <a className="nav-link" href="/callback-list">
                                            Callback list <span className="sr-only">(current)</span>
                                        </a>
                                    </li>
                                    <li className="nav-item">
                                        {
                                            this.context.isAuth() ? (
                                                <button className="btn-sm btn btn-light" onClick={this.handleLogout}>
                                                    Logout
                                                </button>
                                            ):(
                                                <a className="nav-link" href="/login">
                                                    Login
                                                </a>
                                            )
                                        }

                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>

                </nav>
            </div>
        );
    }
}

export default Nav;