import React, {Component, useContext} from 'react';
import {AuthenticateConsumer, AuthenticateContext} from "../../Context/Login/AuthenticateContext";
import axios from "axios";
import config from "../../config";
import { Redirect, Route } from 'react-router-dom'

class Login extends Component {
    static contextType = AuthenticateContext;
    state = {
        formControls: {
            email: {
                value: ""
            },
            password: {
                value: ""
            }
        },
        errors: { },
    }

    changeHandler = event => {
        const name = event.target.name;
        const value = event.target.value;

        this.setState({
            formControls: {
                ...this.state.formControls,
                [name]: {
                    ...this.state.formControls[name],
                    value
                }
            }
        });

    }

    handleLogin = event =>{
        event.preventDefault();

        const data = {
            username: this.state.formControls.email.value,
            password: this.state.formControls.password.value
        }

        axios.post(`${config().api.login}`, data)
            .then(res => {
                console.log(res.data)
                this.context.login(res.data)

            }).catch((error) => {

            this.setState({errors: error.response.data.errors})
        });
    }

    render() {

        if(this.context.isAuth()){
            return(
                <Redirect to='/callback-list' />
            )
        }

        return (

                <React.Fragment>

                    <form className="login-form">


                        <div className="form-row">
                            <div className="col-md-6 mb-3">
                                <label htmlFor="validationCustom03">Email</label>
                                <input type="text"
                                       name="email"
                                       defaultValue={this.state.formControls.email.value}
                                       onChange={this.changeHandler}
                                       className="form-control"
                                       id="validationCustom03"
                                       placeholder=""
                                       required />
                            </div>

                        </div>
                        <div className="form-row">
                            <div className="col-md-6 mb-3">
                                <label htmlFor="validationCustom01">Password</label>
                                <input type="password"
                                       name="password"
                                       defaultValue={this.state.formControls.password.value}
                                       onChange={this.changeHandler}
                                       className="form-control"
                                       id="validationCustom01"
                                       placeholder=""
                                       required />
                            </div>
                        </div>
                        <button className="btn btn-primary btn-sm" onClick={this.handleLogin}>Login</button>
                    </form>

                </React.Fragment>

        );
    }
}

export default Login;