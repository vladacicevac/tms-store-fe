import React, {Component} from 'react';
import DateTime from "./DateTime";
import axios from 'axios';
import DatePicker from "react-datepicker";
import {setHours, setMinutes} from "date-fns";
import cloneDeep from 'lodash/cloneDeep';
import config from "../../config";


const inputParser = {
    formatFormData(formData){
        const data = {};

        Object.keys(formData).map((key, value) => {
            data[key] = formData[key].value;
        });


        return data;
    }
}

const INITIAL_STATE = {
    formControls: {
        name: {
            value: ""
        },
        email: {
            value: ""
        },
        phone: {
            value: ""
        },
        gender: {
            value: ""
        },
        order_no: {
            value: ""
        },
        description: {
            value: ""
        },
        time_for_callback: {
            value: new Date(),
            minTime: '',
            maxTime: ''

        }
    },
    message: {},
    errors: { },
    agreeToTerms: false
}

class CallBackForm extends Component {


    state = { ...INITIAL_STATE }

    componentDidMount() {
        this.baseState = cloneDeep(this.state);

        this.handleTimeForCallback(new Date());
    }

    resetForm = () => {

        this.setState(...INITIAL_STATE)
    }

    handleTimeForCallback = (value) => {

        let minTime =  setHours(setMinutes(new Date(value), 0), 8);
        let maxTime = value.getDay() === 6 ?
            setHours(setMinutes(new Date(value), 0), 13) :
            setHours(setMinutes(new Date(value), 0), 20);

        this.setState({
            formControls: {
                ...this.state.formControls,
                time_for_callback: {
                    ...this.state.formControls['time_for_callback'],
                    minTime,
                    maxTime,
                    value
                }
            }
        });

    }


    handleSubmit = (event) =>{
        event.preventDefault();
        const data = {
            ...this.state.formControls
        }

        const agree = this.state.agreeToTerms;

        if(!agree){
            this.setState({errors: {
                    agree: ["You have to agree with terms and conditions"]
                }
            });
        }else{
            const formData = inputParser.formatFormData(data);

            axios.post(`${config().api.callbacks}`, formData)
                .then(res => {
                    //this.resetForm();

                    this.setState({message: res.response.response_message})

                }).catch(error => {


                    this.setState({errors: error.response.data.errors})
                });
        }



    }

    changeHandler = event => {
        const name = event.target.name;
        const value = event.target.value;

        this.setState({
            formControls: {
                ...this.state.formControls,
                [name]: {
                    ...this.state.formControls[name],
                    value
                }
            }
        });

    }

    handleChecked = event => {
        this.setState({agreeToTerms: !this.state.agreeToTerms});
    }

    render() {

        const isWorkday = date => {

            const day = date.getDay();

            return day !== 0;
        };

        const isErrorsExists = () =>{
            const errors = this.state.errors;

            return Object.keys(errors).length;
        }

        return (
            <React.Fragment>

                <form className="callback-form">

                    {
                        isErrorsExists() ? (
                            <div  className="alert alert-danger" role="alert">
                                {Object.keys(this.state.errors).map(key => {
                                        return (

                                            <p key={key}>
                                                {this.state.errors[key][0]}
                                            </p>

                                        );
                                    })
                                }
                            </div>
                        ) : null
                    }


                    <div className="form-row">
                        <div className="col-md-6 mb-4">
                            <label htmlFor="validationCustom01">Name</label>
                            <input type="text"
                                   name="name"
                                   className="form-control"
                                   id="validationCustom01"
                                   placeholder=""
                                   defaultValue={this.state.formControls.name.value}
                                   onChange={this.changeHandler}
                                   required />
                        </div>
                    </div>
                    <div className="form-row">
                        <div className="col-md-6 mb-4">
                            <label htmlFor="validationCustom01">Gender</label>
                            <select className="form-control"
                                    onChange={this.changeHandler}
                                    onBlur={this.changeHandler}
                                    defaultValue={this.state.formControls.gender.value}
                                    name="gender"
                                    >
                                <option value="">Select gender</option>
                                <option key="male" value="male">Male</option>
                                <option key="female" value="female">Female</option>
                            </select>
                        </div>
                    </div>
                    <div className="form-row">
                        <div className="col-md-6 mb-3">
                            <label htmlFor="validationCustom03">Phone</label>
                            <input type="text"
                                   name="phone"
                                   defaultValue={this.state.formControls.phone.value}
                                   onChange={this.changeHandler}
                                   className="form-control"
                                   id="validationCustom03"
                                   placeholder=""
                                   required />
                        </div>

                    </div>
                    <div className="form-row">
                        <div className="col-md-6 mb-3">
                            <label htmlFor="validationCustom03">Email</label>
                            <input type="text"
                                   name="email"
                                   defaultValue={this.state.formControls.email.value}
                                   onChange={this.changeHandler}
                                   className="form-control"
                                   id="validationCustom03"
                                   placeholder=""
                                   required />
                        </div>

                    </div>
                    <div className="form-row">
                        <div className="col-md-6 mb-3">
                            <label htmlFor="validationCustom03">Order Number</label>
                            <input type="text"
                                   className="form-control"
                                   id="validationCustom03"
                                   placeholder=""
                                   name="order_no"
                                   defaultValue={this.state.formControls.order_no.value}
                                   onChange={this.changeHandler}
                                   required />
                        </div>
                    </div>
                    <div className="form-row">
                        <div className="col-md-6 mb-3">
                            <label htmlFor="validationCustom03">Date and Time for a callback</label>
                            <DatePicker
                                className="form-control"
                                selected={this.state.formControls.time_for_callback.value}
                                onChange={this.handleTimeForCallback}
                                filterDate={isWorkday}
                                placeholderText="Select a weekday"
                                showTimeSelect
                                timeFormat="HH:mm"
                                timeIntervals={30}
                                timeCaption="Time"
                                dateFormat="MMMM d, yyyy h:mm aa"
                                minTime={this.state.formControls.time_for_callback.minTime}
                                maxTime={this.state.formControls.time_for_callback.maxTime}
                                minDate={new Date()}

                            />
                        </div>
                    </div>
                    <div className="form-row">
                        <div className="col-md-6 mb-3">
                            <label htmlFor="validationCustom03">Description</label>
                            <textarea type="text"
                                      className="form-control"
                                      id="validationCustom03"
                                      placeholder=""
                                      name="description"
                                      defaultValue={this.state.formControls.description.value}
                                      onChange={this.changeHandler}
                                      required></textarea>
                        </div>
                    </div>
                    <div className="form-group">
                        <div className="form-check">
                            <input name="agree"
                                   className="form-check-input"
                                   type="checkbox"
                                   value=""
                                   onChange={this.handleChecked}
                                   id="invalidCheck"
                                   required />
                            <label className="form-check-label" htmlFor="invalidCheck">
                                Agree to terms and conditions
                            </label>
                        </div>
                    </div>
                    <button className="btn btn-primary btn-sm" onClick={this.handleSubmit}>Submit form</button>
                </form>
            </React.Fragment>
        );
    }
}

export default CallBackForm;