import React, {Component} from 'react';
import axios from "axios";
import config from "../../config";
import {AuthenticateContext} from "../../Context/Login/AuthenticateContext";
import {Redirect} from "react-router";

class AddComment extends Component {

    static contextType = AuthenticateContext;

    state = {
        name: "",
        gender:"",
        email: "",
        phone: "",
        description: "",
        time_for_callback: "",
        comment: "",
        comments: {},
    }

    componentDidMount() {

        setTimeout(() => {
            if(this.context.isAuth()){
                this.fetchForm();
            }
        }, 500)

    }

    fetchForm() {

        const configuration = {
            headers: {'Authorization': "Bearer " + this.context.authUser.access_token}
        };

        axios.get(`${config().api.callbacks}/${this.props.match.params.id}`, configuration)
            .then(res => {
                const callbackForm = res.data.data;
                console.log(callbackForm);
                this.setState({
                    name: callbackForm.name,
                    gender:callbackForm.gender,
                    email: callbackForm.email,
                    phone: callbackForm.phone,
                    description: callbackForm.description,
                    time_for_callback: callbackForm.time_for_callback,
                    comments: callbackForm.comments,
                });
            }).catch(err => {

        });
    }

    handleComment = (event) =>{
        this.setState({comment: event.target.value})
    }

    addComment = (event) => {
        const configuration = {
            headers: {'Authorization': "Bearer " + this.context.authUser.access_token}
        };

        const data = {
            comment: this.state.comment
        }

        axios.post(`${config().api.callbacks}/${this.props.match.params.id}/add-comment`, data, configuration)
            .then(res => {
                const callbackForm = res.data.data;
                console.log(callbackForm);
                this.setState({
                    name: callbackForm.name,
                    gender:callbackForm.gender,
                    email: callbackForm.email,
                    phone: callbackForm.phone,
                    description: callbackForm.description,
                    time_for_callback: callbackForm.time_for_callback,
                    comments: callbackForm.comments,
                    comment: ""
                });
            }).catch(err => {

        });
    }

    render() {

        return (
            <React.Fragment>
            <div className="col-lg-6">
                <h5>Name: {this.state.name}</h5>

                <h5>Email: {this.state.name}</h5>
                <h5>Phone: {this.state.phone}</h5>
                <h5>Description: {this.state.description}</h5>
                <h5>Time for call: {this.state.time_for_callback}</h5>
                <h5>Comments: </h5>
                {Object.keys(this.state.comments).map(key => {
                    return (

                        <div key={key} className="card">
                            <div className="card-body">
                                {this.state.comments[key].comment}
                            </div>
                        </div>

                    );
                })
                }
                <textarea className="form-control" placeholder="Enter comments" defaultValue={this.state.comment} onChange={this.handleComment} name="comment">

                </textarea>
                <br />
                <button onClick={this.addComment} className="btn btn-primary">Add comment</button>
            </div>
            </React.Fragment>
        );
    }
}

export default AddComment;