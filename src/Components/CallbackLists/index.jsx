import React, {Component} from 'react';
import axios from 'axios';
import config from "../../config"
import {AuthenticateContext} from "../../Context/Login/AuthenticateContext";
import {Redirect} from "react-router";

class CallbackLists extends Component {

    static contextType = AuthenticateContext;

    state = {
        callbackList: [],
        messages: {}
    }

    componentDidMount() {
        console.log(this.context.isAuth());
        if(this.context.isAuth()){
            this.getCallbackList();
        }

    }

    getCallbackList(){

        const configuration = {
            headers: {'Authorization': "Bearer " + this.context.authUser.access_token}
        };

        axios.get(`${config().api.callbacks}`, configuration)
            .then(res => {
                const callbackList = res.data.data;
                this.setState({ callbackList });
            }).catch(err => {

            })
    }

    deleteCallbackListItem(id, key){
        const configuration = {
            headers: {'Authorization': "Bearer " + this.context.authUser.access_token}
        };

        axios.delete(`${config().api.callbacks}/${id}`, configuration)
            .then(res => {
                this.deleteRow(key);
            })
    }

    deleteRow = (index) => {
        let callbackList = [...this.state.callbackList];
        callbackList.splice(index, 1);
        this.setState({callbackList: callbackList, messages: ['Record is deleted.']});
    }



    render() {

        if(!this.context.isAuth()){
            return(
                <Redirect to='/login' />
            )
        }

        const isErrorsExists = () =>{
            const messages = this.state.messages;

            return Object.keys(messages).length;
        }

        return (
            <React.Fragment>
                {
                    isErrorsExists() ? (
                        <div  className="alert alert-danger" role="alert">
                            {Object.keys(this.state.messages).map(key => {
                                return (

                                    <p key={key}>
                                        {this.state.messages[key]}
                                    </p>

                                );
                            })
                            }
                        </div>
                    ) : null
                }
                <table className="table">
                    <thead>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Name</th>
                        <th scope="col">Email</th>
                        <th scope="col">Phone</th>
                        <th scope="col">Schedule to call</th>
                        <th scope="col">Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    {Object.keys(this.state.callbackList).map(key => {

                        return (
                            <tr index={key} key={key}>
                                <th scope="row">{this.state.callbackList[key].id}</th>
                                <td>{this.state.callbackList[key].name}</td>
                                <td>{this.state.callbackList[key].email}</td>
                                <td>{this.state.callbackList[key].phone}</td>
                                <td>{this.state.callbackList[key].time_for_callback}</td>
                                <td>
                                    <a   key={this.state.callbackList[key].id} href={`/add-comment/${this.state.callbackList[key].id}`} className="btn btn-sm btn-primary">Add comment</a>
                                    <button type="button" key={key} onClick={() => this.deleteCallbackListItem(this.state.callbackList[key].id, key)} className="btn btn-sm btn-dark">Archive</button>
                                </td>
                            </tr>
                        );
                    })}


                    </tbody>
                </table>
            </React.Fragment>
        );
    }
}

export default CallbackLists;